/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package org.apache.cordova.lockdetect;

import java.util.TimeZone;

import android.app.admin.DevicePolicyManager;
import android.content.Context;
import java.lang.reflect.Method;
import android.util.Log;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.provider.Settings;

public class LockDetect extends CordovaPlugin {
    public static final String TAG = "LockDetect";

    public static String platform;                            // Device OS


    /**
     * Constructor.
     */
    public LockDetect() {
    }

    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action            The action to execute.
     * @param args              JSONArry of arguments for the plugin.
     * @param callbackContext   The callback id used when calling back into JavaScript.
     * @return                  True if the action was valid, false if not.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("isDeviceSecured")) {
            JSONObject r = new JSONObject();
            r.put("isSecured", this.isDeviceSecured());
            callbackContext.success(r);
        }
        else {
            return false;
        }
        return true;
    }

    //--------------------------------------------------------------------------
    // LOCAL METHODS
    //--------------------------------------------------------------------------

    public boolean isDeviceSecured()
    {
        Log.v("LockDetect", "isDeviceSecured runtime");
        String LOCKSCREEN_UTILS = "com.android.internal.widget.LockPatternUtils";
        Context context=this.cordova.getActivity().getApplicationContext();

        try
        {
            Class<?> lockUtilsClass = Class.forName(LOCKSCREEN_UTILS);
            // "this" is a Context, in my case an Activity
            Object lockUtils = lockUtilsClass.getConstructor(Context.class).newInstance(context);

            Method method = lockUtilsClass.getMethod("isLockScreenDisabled");

            boolean isDisabled = Boolean.valueOf(String.valueOf(method.invoke(lockUtils)));

            return isDisabled;
        }
        catch (Exception e)
        {
            Log.e("reflectInternalUtils", "ex:"+e);
        }

        return false;
    }


}
